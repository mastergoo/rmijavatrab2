/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author aluno
 */
public class FiboServer implements Fibonacci {

        private String host;

    public static void main(String[] args) {
        try {
            FiboServer server = new FiboServer();
            server.host = args[0];
            Fibonacci stubfibo = (Fibonacci) UnicastRemoteObject.exportObject(server, 0);
            Registry reg = LocateRegistry.getRegistry(server.host);
            reg.rebind("//" + server.host + "/fibo", stubfibo);
            System.err.println("Servidor carregado");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getN(int n) throws RemoteException{
                long a = 0, b = 1;
                while(n-- > 0){
                        long next = a + b;
                        a = b;
                        b = next;
                }
                return b;
    }

}

