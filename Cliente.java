/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Cliente {

        public static void main(String[] args) {
                try {
                        String serverip = args[0];
                        Registry registryip = LocateRegistry.getRegistry(serverip);
                        Ip stubip = (Ip) registryip.lookup("//" + serverip + "/ip");
                        String ip_remote_fibo = stubip.getIp();

                        int n = Integer.parseInt(args[1]);
                        Registry registryfibo = LocateRegistry.getRegistry(ip_remote_fibo);
                        Fibonacci stubfibo = (Fibonacci) registryfibo.lookup("//" + ip_remote_fibo + "/fibo"); 
                        System.out.println(stubfibo.getN(n));
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }
}






