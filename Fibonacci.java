/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author aluno
 */
public interface Fibonacci extends Remote {

    public long getN(int n) throws RemoteException;
}

