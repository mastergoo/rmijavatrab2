/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author aluno
 */
public class IpServer implements Ip {

    private String iphost;
        private final String[] ipsFibo = {"10.30.16.139","10.30.16.211","10.30.16.72"};
        private int idxServer = 0;

    public static void main(String[] args) {
        try {
            IpServer server = new IpServer();
            server.iphost = args[0];
            Ip stubIp = (Ip) UnicastRemoteObject.exportObject(server, 0);
            Registry reg = LocateRegistry.getRegistry(server.iphost);
            reg.rebind("//" + server.iphost + "/ip", stubIp);
            System.err.println("Servidor carregado");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getIp() throws RemoteException{
                int tmp = idxServer;
                idxServer = (idxServer+1) % 3;
                return ipsFibo[tmp];
    }

}

